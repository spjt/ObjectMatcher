# ObjectMatcher
Utility to determine whether a given Java object matches the fields defined in another object.

For instance, if you have a persistence class called Car with fields like make and model, you can create a Car with only make set as a test object. Then, using ObjectMatcher.matchesAllDefined(myCar, testObject) will return true if myCar has that make, regardless of any other fields that are set on myCar.

If the class contains other objects, they will be compared recursively, e.g. if Car has a owner field of type Owner that has a firstName and lastName, you can create a Car with no fields set, an Owner with only the lastName set, set the Car's Owner to that Owner, and then match against a Car. 

When comparing collections/arrays, the object under test is considered to match if the corresponding collection contains all of the elements defined in the test object, so if the myCar has a List<Feature> containing CruiseControl, AirConditioning and TurboBoost, testObject will match if it contains only CruiseControl (but not if it contains OilSlick). 
