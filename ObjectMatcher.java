import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by phil on 4/1/15.
 *
 * Utilities to test if an object matches a certain criteria.
 */
public class ObjectMatcher {

	/**
	 * Determines whether the underTest object has all of the same fields
	 * as the test object, for all fields that are defined in test.
	 *
	 * For collections and arrays, the test will pass if the corresponding
	 * collection/array in underTest contains all of the elements present in
	 * test.
	 *
	 * @param test
	 * @param underTest
	 * @param <T>
	 * @return
	 */
	public static <T> boolean matchesAllDefined(T test, T underTest) {
		Field[] fields = test.getClass().getFields();
		Field[] declaredFields = test.getClass().getDeclaredFields();

		List<Field> allFields = new ArrayList<Field>();
		Collections.addAll(allFields, fields);
		Collections.addAll(allFields, declaredFields);

		for (Field f : allFields) {
			boolean saveAccessible = f.isAccessible();
			f.setAccessible(true);
			try {
				if (f.get(test) == null) {
					// Do not try to compare fields that are not set.
					continue;
				} else if (f.get(test) instanceof Object[] || f.get(test) instanceof Collection) {
					if (f.get(underTest) == null) {
						// If f.get(test) is an instanceof anything, it is not null.
						return false;
					}
					HashSet<Object> testSet = new HashSet<Object>();
					HashSet<Object> underTestSet = new HashSet<Object>();
					if(f.get(test) instanceof Object[]) {
						Collections.addAll(testSet, f.get(test));
						Collections.addAll(underTestSet, f.get(underTest));
					}
					if(f.get(test) instanceof Collection) {
						testSet.addAll((Collection<?>) f.get(test));
						underTestSet.addAll((Collection<?>) f.get(underTest));
					}
					for(Object testObject : testSet) {
						boolean foundMatch = false;
						for(Object underTestObject : underTestSet) {
							if(ObjectMatcher.matchesAllDefined(testObject, underTestObject)) {
								foundMatch = true;
								break;
							}
						}
						if(!foundMatch) {
							return false;
						}
					}
				} else if (!compareDirectly(f.getType())) {
					if (!matchesAllDefined(f.get(test), f.get(underTest))) {
						return false;
					}
				} else if (!f.get(test).equals(f.get(underTest))) {
					return false;
				}
			} catch (IllegalAccessException e) {
				// Ignore any fields that cause an error.
			} finally {
				f.setAccessible(saveAccessible);
			}
		}
		return true;
	}

	/**
	 * Returns true if instances should be compared with .equals or
	 * whether they should be recursively compared. e.g. primitives,
	 * (which will be boxed due to reflection returning an object)
	 * strings, uuid, etc. should be compared with .equals.
	 *
	 * If you are getting StackOverflow exceptions when doing comparisons,
	 * you probably need to add something here. A convenient way to do this
	 * is if your objects are all in the same package root, just use
	 * c.getCanonicalName and only return false if the object is in your package.
	 * @param c
	 * @return
	 */
	private static boolean compareDirectly(Class c) {
		if(c.isPrimitive()) { return true; }
		Set<Class> directCompares = new HashSet<Class>() {{
			add(UUID.class);
		}};
		if(directCompares.contains(c)) { return true; }
		if(c.getCanonicalName().startsWith("java")) { return true; }
		return false;
	}

}
