import java.util.List;

/**
 * Created by phil on 4/6/15.
 */
public class OMTest {

	public int _int;
	public int[] intArray;
	public List<Integer> intList;

	public String string;
	public List<String> stringList;
	public String[] stringArray;

	public OMTest omTest;
	public OMTest[] omTestArray;
	public List<OMTest> omTestList;

}
