
import scala.collection.JavaConversions._

def fillBaseFields(t: OMTest) = {
  t._int = 0
  t.intArray = Array(1, 2, 3)
  t.intList = new java.util.ArrayList[Integer](List[Integer](1, 2, 3))
  t.string = "String"
  t.stringArray = Array("String1", "String2", "String3")
  t.stringList = new java.util.ArrayList[String](List[String]("String1", "String2", "String3"))
}

def reset(t: OMTest) = {
  fillBaseFields(t)
  t.omTest = new OMTest
  fillBaseFields(t.omTest)
  t
}

val baseOM = reset(new OMTest)

def testMatch(f: => OMTest): Boolean = {
  ObjectMatcher.matchesAllDefined(f, baseOM)
}
def assertMatch(f: => OMTest, msg: String = ""): Unit = assert(testMatch(f), msg)
def assertNoMatch(f: => OMTest, msg: String = ""): Unit = assert(!testMatch(f), msg)

val om = new OMTest
// No fields defined, should match.
assertMatch(om)

// Fill in the same fields and check for match.
reset(om)
assertMatch(om)

// Change the integer, shouldn't match.
om._int = 5
assertNoMatch(om)

// Remove one string from the list. It should still match because
// baseOM contains all of the elements still present.
reset(om).stringList.remove(0)
assertMatch(om)

// Add a new string to the list. Should no longer match.
om.stringList.add("New")
assertNoMatch(om)

// Change the integer in the nested object.
reset(om)
om.omTest._int = 5
assertNoMatch(om)

